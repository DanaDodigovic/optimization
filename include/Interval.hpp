#ifndef INTERVAL_HPP
#define INTERVAL_HPP

#include <iostream>

struct Interval {
    double a, b;

    Interval(double a, double b);
    friend std::ostream& operator<<(std::ostream& out, const Interval& i);
    friend bool operator==(const Interval& first, const Interval& second);
};

#endif

#ifndef CONSTRAINT_HPP
#define CONSTRAINT_HPP

#include "Point.hpp"

#include <vector>
#include <utility>
#include <memory>

class Bound {
  public:
    double lower;
    double upper;

  public:
    Bound(double lower, double upper);
};

class ExplicitConstraint {
  private:
    std::vector<Bound> bounds;

  public:
    ExplicitConstraint(std::vector<Bound> bounds);
    ExplicitConstraint(double lower_bounds, double upper_bounds, unsigned n);
    bool is_satisfied(const Point& x) const;
    void set_on_bound(Point& x);
    Point get_lower() const;
    Point get_upper() const;
};

class ImplicitConstraint {
    // private:
    // Point x;
    // public:
};

class EquationConstraint : public ImplicitConstraint {
  public:
    double operator()(const Point& x);
    bool is_satisfied(const Point& x) const;
    double h_(const Point& x) const;

  private:
    virtual bool is_satisfied_(const Point& x) const = 0;
    virtual double h(const Point& x) const           = 0;
};

class EquationConstraints {
  private:
    std::vector<std::unique_ptr<EquationConstraint>> constraints;

  public:
    bool are_satisfied(const Point& x) const;
    void add(std::unique_ptr<EquationConstraint> constraint);

    std::vector<std::unique_ptr<EquationConstraint>>::const_iterator
    begin() const;

    std::vector<std::unique_ptr<EquationConstraint>>::const_iterator
    end() const;
};

class InequalityConstraint : public ImplicitConstraint {
  public:
    double operator()(const Point& x);
    bool is_satisfied(const Point& x) const;
    double g_(const Point& x) const;

  private:
    virtual bool is_satisfied_(const Point& x) const = 0;
    virtual double g(const Point& x) const           = 0;
};

class InequalityConstraints {
  private:
    std::vector<std::unique_ptr<InequalityConstraint>> constraints;

  public:
    void add(std::unique_ptr<InequalityConstraint> constraint);
    bool are_satisfied(const Point& x) const;

    std::vector<std::unique_ptr<InequalityConstraint>>::const_iterator
    begin() const;

    std::vector<std::unique_ptr<InequalityConstraint>>::const_iterator
    end() const;
};

/* Constraints. */
class G1 : public InequalityConstraint {
  private:
    bool is_satisfied_(const Point& x) const override;
    double g(const Point& x) const override;
};

class G2 : public InequalityConstraint {
  private:
    bool is_satisfied_(const Point& x) const override;
    double g(const Point& x) const override;
};

class G3 : public InequalityConstraint {
  private:
    bool is_satisfied_(const Point& x) const override;
    double g(const Point& x) const override;
};

class G4 : public InequalityConstraint {
  private:
    bool is_satisfied_(const Point& x) const override;
    double g(const Point& x) const override;
};

class H1 : public EquationConstraint {
  private:
    bool is_satisfied_(const Point& x) const override;
    double h(const Point& x) const override;
};

#endif

#include "Interval.hpp"

Interval::Interval(double a, double b) : a(a), b(b) {}

std::ostream& operator<<(std::ostream& out, const Interval& i)
{
    out << "[" << i.a << ", " << i.b << "]" << std::endl;
    return out;
}

bool operator==(const Interval& first, const Interval& second)
{
    constexpr double epsilon = 1e-2;
    return std::abs(first.a - second.a) < epsilon &&
           std::abs(first.b - second.b) < epsilon;
}
